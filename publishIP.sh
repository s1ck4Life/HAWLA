#!/bin/bash

cd ~pi/HAWLA

ifconfig | grep "inet Adresse:" | awk '{print$2;exit}' | sed -e s/Adresse:// > raspberrypi.ip

git add raspberrypi.ip
git commit -a -m "New local IPv4 for RaspberryPi"
git push