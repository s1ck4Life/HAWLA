# When using raspberry pi in haw network.. some ports are blocked. so is the ntp port blocked
# with the following script we can get time from an api and set current system time
# this is important because certificates are not accepted if they only valid in about 47 years..
time=$(wget http://www.timeapi.org/utc/in+two+hours?format=%25d%20%25b%20%25Y%20%25I:%25M:%25S -q -O -)
echo "Time set to:"
sudo date -s "`echo $time`"
