'''
Test BLE Scanner
author: Daniel Hilpoltsteiner
'''

# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Example Python node to publish on a specific topic.
"""
    '''
    Ros node bluetooth class.
    '''

# Import required Python code.
import rospy

# Give ourselves the ability to run a dynamic reconfigure server.
from dynamic_reconfigure.server import Server as DynamicReconfigureServer

# Import custom message data and dynamic reconfigure variables.
from node_example.msg import bluetooth
from node_example.cfg import nodeExampleConfig as ConfigType

import blescan, commands
import sys, os, json

import bluetooth._bluetooth as bluez


class BeaconNode:

    def __init__(self):
        # Get the private namespace parameters from command line or launch file.
        init_message = rospy.get_param('~message', 'hello')
        rate = float(rospy.get_param('~rate', '1.0'))
        rospy.loginfo('rate = %f', rate)
        # Create a dynamic reconfigure server.
        self.server = DynamicReconfigureServer(ConfigType, self.reconfigure_cb)
        # Create a publisher for our custom message.
        self.pub = rospy.Publisher('example', bluetooth, queue_size=10)
        # Initialize message variables.
        self.int_a = 1
        self.int_b = 2
        self.message = init_message

        # Create a timer to go to a callback. This is more accurate than
        # sleeping for a specified time.
        rospy.Timer(rospy.Duration(1 / rate), self.timer_cb)

        # Allow ROS to go to all callbacks.
        rospy.spin()

    direction = ""
    start = 0
    destination = 0

    direction = ""

    def find_room(self, beacon):
        with open('../roomdata.json') as data_file:
            roomdata = json.load(data_file)
        for room in roomdata:
            if room['beacon'] == beacon.identifier:
                return float(room["room_no"])


    def beacon_algorithm(self, beacons):
        nearest_beacon = beacons[0]
        second_nearest_beacon = beacons[1]

        max_distance = abs(self.start-self.destination)

        current_distance = self.destination - self.find_room(nearest_beacon)

        if(current_distance > 0):
            print ' Go Right'
            self.direction = commands.Right
        elif current_distance < 0:
            print ' Go Left'
            self.direction = commands.Left
        else:
            print 'Near the Destination'
            distance = self.destination - self.find_room(second_nearest_beacon)
            if distance > 0:
                print 'Left of Destination'
                self.direction = commands.Right
            if distance < 0:
                print 'Right of Destination'
                self.direction = commands.Left



    def timer_cb(self, _event):
        """
        Called at a specified interval. Publishes message.
        """
        # Set the message to publish as our custom message.


        dev_id = 0
        try:
            sock = bluez.hci_open_dev(dev_id)
            print "BLE THREAD STARTED"

        except:
            print "ERROR ACCESSING BLUETOOTH.."
            sys.exit(1)

        blescan.hci_le_set_scan_parameters(sock)
        blescan.hci_enable_le_scan(sock)

        beacons = blescan.parse_events(sock, 10)
        print "----------"

        sortedBeacons = sorted(beacons, key=lambda beacon: beacon.rssi)

        beacon_algorithm(sortedBeacons)

        for beacon in beacons:
            type(beacon)
            msg = bluetooth()

            # Fill in custom message variables with values updated from dynamic
            # reconfigure server.
            msg.uuid = beacon.uuid
            msg.major = beacon.major
            msg.minor = beacon.minor

            # Publish our custom message.
            self.pub.publish(msg)

            print beacon


    def reconfigure_cb(self, config, dummy):
        '''
        Create a callback function for the dynamic reconfigure server.
        '''
        # Fill in local variables with values received from dynamic reconfigure
        # clients (typically the GUI).
        self.message = config["message"]
        self.int_a = config["a"]
        self.int_b = config["b"]
        # Return the new variables.
        return config

# Main function.
if __name__ == '__main__':
    # Initialize the node and name it.
    rospy.init_node('bluetoothSensor')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        BeaconNode()
    except rospy.ROSInterruptException:
        pass



