'''
Test BLE Scanner
author: Daniel Hilpoltsteiner
'''

import blescan
import sys, os, json

import bluetooth._bluetooth as bluez


def nearestRoom( nearestBeacon, roomdata):
    for room in roomdata:
        if room['beacon'] == nearestBeacon.identifier:
            return room["room_no"]


dev_id = 0
try:
    sock = bluez.hci_open_dev(dev_id)
    print "BLE THREAD STARTED"

except:
    print "ERROR ACCESSING BLUETOOTH.."
    sys.exit(1)

blescan.hci_le_set_scan_parameters(sock)
blescan.hci_enable_le_scan(sock)

while True:
    beacons = blescan.parse_events(sock, 10)
    print "----------"

    sortedBeacons = sorted(beacons, key=lambda beacon: beacon.rssi)
    for beacon in sortedBeacons:
        type(beacon)
        print beacon

    nearestBeacon = sortedBeacons[0]
    with open('../roomdata.json') as data_file:
        roomdata = json.loads(data_file)
    room = nearestRoom(nearestBeacon, roomdata)
    print room




