from django.contrib.auth import (
    login as django_login,
    logout as django_logout
)
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
import json, os

from django.views.decorators.csrf import csrf_exempt

MODULE_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

CURRENT_DATA_FILENAME = 'roomdata.json'
CURRENT_DATA_FILE = open(os.path.join(MODULE_DIR, CURRENT_DATA_FILENAME)).read()

def roomdata(request):
    CURRENT_DATA_FILE = open(os.path.join(MODULE_DIR, CURRENT_DATA_FILENAME), 'r').read()
    print CURRENT_DATA_FILE
    return HttpResponse(CURRENT_DATA_FILE)
    #return HttpResponse(json.dumps(get_roomdata()))


@csrf_exempt
def routine(request):
    print request
    print request.body
    print 'routine'
    order = open('order.json', 'w+')
    order.write(request.body)
    return HttpResponse(request.body)


from bs4 import BeautifulSoup
import urllib, json


def get_roomdata():
    r = urllib.urlopen('https://www.haw-landshut.de/hochschule/fakultaeten/informatik/wir-ueber-uns/professoren-innen.html').read()
    #r = urllib.urlopen('https://www.haw-landshut.de/hochschule/fakultaeten/informatik/wir-ueber-uns.html').read()
    soup = BeautifulSoup(r)
    # Get all names
    names = soup.find_all("h3")
    rooms = soup.find_all("div", class_="ui_text_long")

    result = []
    UUID = "efd08370a247c89837e7b5634df555"
    MAJOR = 212
    MINOR = 0

    for x in xrange(names.__len__()):
        # This is magic: trust me!
        room = rooms[x].p.get_text().split("\n")[0].encode('ascii', errors='ignore').strip()
        if room.__len__() > 1:
            if len(room.split(": ")[1]) > 1:
                element = {}
                element["name"] = names[x].get_text().strip()
                element["room"] = room.split(": ")[1]
                # if len(element["room"].split(" ")) > 1:
                element["room_no"] = element["room"].split(" ")[1]
                # else:
                #     element["room_no"] = 9999
                element["beacon"] = element["beacon"] = "{0}-{1}-{2}".format(UUID, MAJOR, MINOR)
        MINOR += 1
        result.append(element)

    result = sorted(result, key=lambda entry: entry["room_no"])
    print json.dumps(result)

    return result
